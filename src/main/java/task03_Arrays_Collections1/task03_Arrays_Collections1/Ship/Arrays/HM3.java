package task03_Arrays_Collections1.Ship.Arrays;

import java.lang.reflect.Array;
import java.util.Arrays;

public class HM3 {
    public static void main(String[] args) {
        Container container = new Container();
//        container.setStringArray(new int[]{1432});
//        System.out.println(Arrays.toString(container.stringArray));
//        container.addString(container.stringArray, new int[]{3234});

        Container.Methodika methodika = new Container.Methodika();

        container.setNumArray(0,methodika.changeStringToArrat("A"));
        container.setNumArray(1,methodika.changeStringToArrat("S"));
        container.setNumArray(2,methodika.changeStringToArrat("V"));
        container.setNumArray(3,methodika.changeStringToArrat("A"));
    }
}