package task03_Arrays_Collections1.Ship.Game;

import java.util.Objects;

public class Dors {
   int num;

    public Dors(int num) {
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dors dors = (Dors) o;
        return num == dors.num;
    }

    @Override
    public int hashCode() {
        return Objects.hash(num);
    }

    @Override
    public String toString() {
        return "Dors{" +
                "num=" + num +
                '}';
    }
}
