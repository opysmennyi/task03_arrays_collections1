package task03_Arrays_Collections1.Ship.Deque;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Deque<String> deq = new ArrayDeque<String>();
        deq.add("REDD'S"); // стандартное добавление элементов
        deq.addFirst("MR. DARK"); // добавляем элемент в самое начало
        deq.addLast("BROWAR NA JURZE"); // добавляем элемент в конец коллекции
        deq.addLast("Kronenbourg 1664"); // добавляем элемент в конец коллекции
        deq.push("AIPA APA"); // добавляем элемент в самое начало
        deq.push("DESTBIR"); // добавляем элемент в самое начало
        deq.offer("SOMERSBY");
        deq.offerFirst("Hoegaarden"); //добавляет элемент obj в самое начало очереди. Если элемент удачно добавлен, возвращает true, иначе - false
        deq.offerLast("OKOCIM"); //добавляет элемент obj в конец очереди. Если элемент удачно добавлен, возвращает true, иначе - false

        System.out.print("Размер массива: ");
        System.out.println(deq.size());
        System.out.println("Элементы массива: " + deq + "\n");
        deq.poll().indexOf(deq.size()); //удаляем первый элемент
        System.out.print("Размер массива: ");
        System.out.println(deq.size());
        System.out.println("Элементы массива: " + deq + "\n");

        boolean okosIs = deq.removeLastOccurrence("OKOCIM"); //удаляет последний встреченный элемент obj из очереди.
        System.out.println(okosIs); //Если удаление произшло, то возвращает true, иначе возвращает false.
        System.out.print("Размер массива: ");
        System.out.println(deq.size());
        System.out.println("Элементы массива: " + deq + "\n");

        System.out.println("__________________________________________");
        System.out.println("Using Iterator");
        System.out.println("------------------------------------------");
        Iterator iterator = deq.iterator();
        while (iterator.hasNext())
            System.out.println("\t" + iterator.next());

        System.out.println("__________________________________________");
        Iterator reverse = deq.descendingIterator();
        System.out.println("Using Reverse Iterator");
        System.out.println("------------------------------------------");
        while (reverse.hasNext())
            System.out.println("\t" + reverse.next());
    }

}

//Размер массива: 9
//Элементы массива: [Hoegaarden, DESTBIR, AIPA APA, MR. DARK, REDD'S, BROWAR NA JURZE, Kronenbourg 1664, SOMERSBY, OKOCIM]