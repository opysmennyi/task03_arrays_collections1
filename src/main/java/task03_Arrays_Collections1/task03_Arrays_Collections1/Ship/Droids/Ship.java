package task03_Arrays_Collections1.Ship.Droids;

import java.util.Objects;
import java.lang.Comparable;

public class Ship  implements Droids, Comparable<Ship> {
    private int distanse;
    int id;
    String name;

    public Ship(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void onAboard() {
        System.out.println("All on a board!");
    }

    public void outOfBoard() {
        System.out.println("get out of here!");
    }

    @Override
    public int compareTo(task03_Arrays_Collections1.Ship.Droids.Ship o) {
        return 0;
    }

    public Ship() {
    }




    public Ship(String name, int distanse) {
        this.name = name;
        this.distanse = distanse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PriorityQue)) return false;
        PriorityQue priorityQue = (PriorityQue) o;
        return getDistanse() == priorityQue.getDistanse() &&
                Objects.equals(getName(), priorityQue.getName());
    }


    @Override
    public int hashCode() {

        return Objects.hash(name, distanse);
    }


    public int getDistanse() {
        return distanse;
    }

    public void setDistanse(int distanse) {
        this.distanse = distanse;
    }

    public void append(String s) {
    }

    @Override
    public String toString() {
        return "Ship{" +
                "distanse=" + distanse +
                ", id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
