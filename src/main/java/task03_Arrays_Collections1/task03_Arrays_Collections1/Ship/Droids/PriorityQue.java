package task03_Arrays_Collections1.Ship.Droids;

import java.util.Objects;
import java.util.*;

     class  PriorityQue<Ship extends Comparable<Ship>> {
        private List<Ship> list;

        public PriorityQue() {
            list = new ArrayList<>();
        }

        public PriorityQue(Ship[] array) {
            list = Arrays.asList(array);
            Collections.sort(list);

        }


        public void add(Ship item) {
            list.add(item);
            Collections.sort(list);
        }

        public void clear() {
            list.clear();
        }

        public boolean contains(Ship item) {
            return list.contains(item);
        }

        public void offer(Ship item) {
            add(item);
        }

        public Ship peek() {
            if (list.size() == 0) {
                return null;
            }

            return list.get(0);
        }

        public Ship poll() {
            if (list.size() == 0) {
                return null;
            }

            Ship result = list.get(0);
            list.remove(0);
            return result;
        }

        public void remote(Ship item) {
            list.remove(item);
        }

        public int size() {
            return list.size();
        }

        public Ship[] toArray() {
            return (Ship[]) list.toArray();
        }

         public Object getName() {
             return null;
         }

         public int getDistanse() {

             return 0;
         }

         @Override
         public String toString() {
             return "PriorityQue{" +
                     "list=" + list +
                     '}';
         }
     }

